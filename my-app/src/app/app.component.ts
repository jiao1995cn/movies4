import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

  sidebarVisible2: boolean = false;
  moviesWithActors: any[] = [];
  movieDetails: any;
  moviesByPerson: any[] = [];
  selectedPersonName: any;



  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.getMoviesWithActors();
  }



  getMoviesWithActors(): void {
    const query = `
    query {
      moviesWithActors {
        title
        poster
      }
    }
    
    `;
    const requestBody = { query };

    this.http.post('http://localhost:3000/api/graphql', requestBody)
      .subscribe((data: any) => {
        this.moviesWithActors = data.data.moviesWithActors;
      });
  }

  showMovieDetails(movieTitle: string): void {
    
    this.sidebarVisible2 = true;
    //console.log(movieTitle)
    this.selectedPersonName = null; 
    this.moviesByPerson = [];
    const query = `
    query {
      movieDetails(title: "${movieTitle}") {
        title
        url
        poster
        plot
        actors {
          name
          url
        }
        directors {
          name  
          url
        }
      }
    }
    `;

    const requestBody = { query };

    this.http.post('http://localhost:3000/api/graphql', requestBody)
      .subscribe((data: any) => {
        //console.log(data.data.movieDetails);
        this.movieDetails = data.data.movieDetails;
      });
  }

  showMoviesByPerson(personName: string): void {
    const query = `
      query {
        moviesByPerson(name: "${personName}") {
          title
          url
        }
      }
    `;
  
    const requestBody = { query };
  
    this.http.post('http://localhost:3000/api/graphql', requestBody)
      .subscribe((data: any) => {
        console.log(data.data.moviesByPerson);
        this.moviesByPerson = data.data.moviesByPerson;
        this.selectedPersonName = personName;
      });
  }


}