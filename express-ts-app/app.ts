import express from 'express';
import cors from 'cors';
import apiRoutes from './apiRoutes';

const app = express();
const port: number = 3000;

app.use(cors());
app.use(cors({
  origin: 'http://localhost:4200',
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
}));

app.use('/api', apiRoutes);

app.listen(port, () => {
  console.log('Server is running on port 3000');
});
